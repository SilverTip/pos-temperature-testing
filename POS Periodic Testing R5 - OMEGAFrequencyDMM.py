#POS Periodic Testing R3
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#POS Periodic Testing

import pyvisa
import csv
import sys
import time
import math
import json
import os
import shutil
import datetime
import socket

GATE_TIME = 1
PERIOD = 60
#HEADER = ['Time','POS1 Frequency', 'POS1 PPB','POS2 Frequency', 'POS2 PPB']
HEADER = ['Time','POS1 Frequency (Hz)', 'POS1 (PPB)', 'V_Tune (V)', 'Temperature']

#TChamber_VISA = 'TCPIP0::10.0.10.151::8888::SOCKET'
TSENSOR_VISA = '10.0.10.34:2000'
FCOUNTER_VISA = 'TCPIP0::10.0.10.98::inst0::INSTR'
DMM_VISA = 'USB0::0x0957::0x0618::MY53090034::0::INSTR'

class Instrument:
    # Applies to all Keysight equipment
    def __init__(self, address: str, idn_query=True):
        self.rm = pyvisa.ResourceManager()
        self.address = address
        self.instr = self.rm.open_resource(self.address)    # Returns Resource subclass, not Resource class itself
        self.instr.timeout = 10000
        self.idn = None
        if idn_query:
            try:
                self.idn = self.get_idn()
            except pyvisa.VisaIOError:
                # try again
                self.idn = self.get_idn()

    def get_idn(self):
        if self.address.count('SOCKET'):
            return 'IDN not found'
        return self.instr.query('*IDN?').split('\n')[0]

    def cls(self, logger=None):
        if logger:
            logger.debug(self.err())
        self.instr.write('*CLS')

    def opc(self):  # Enable Operation Complete flag on machine
        self.instr.write('*OPC')

    def esr(self):  # Check Event Status Register for completed sweep/measurement
        return bool(int(self.instr.query('*ESR?')))

    def err(self) -> tuple:
        if not (self.idn.count('Keysight') or self.idn.count('Agilent') or self.idn.count('WILTRON')):
            return (0, 'ERROR NOT IMPLEMENTED')
        error = self.instr.query('SYST:ERR?').strip('\n').replace('\"', '').split(',')
        error[0] = int(error[0])
        return error

    def get_cal_state(self) -> str:
        # Return In-Cal, Nearing Cal, Out-of-Cal? I don't know what this will look like yet
        bool(self.instr.query('*idn?').split('\n')[0])
        return 'Unknown'

    def close(self):
        self.instr.close()
        self.rm.close()

    def query(self, message: str):
        return self.instr.query(message)

    def write(self, message: str):
        return self.instr.write(message)

class Voltmeter(Instrument):
    def __init__(self, address: str):
        super().__init__(address)
        self.name = None
        if self.idn.count("Keithley"):
            self.name = "KE"
            self.channel = 2
        elif self.idn.count("U3606"):
            self.name = "KS2"
        elif self.idn.count("Keysight"):
            self.name = "KS"
        else:
            self.name = 'HP'
            self.idn = 'AMMETER_IDN_PLACEHOLDER'

    def get_voltage(self):
        if self.name == 'KS2':
            return round(float(self.instr.query('SOURce:SENSe:VOLTage:LEVel?')), 1)
        return float(self.instr.query('READ?'))

class FrequencyCounter(Instrument):
    def __init__(self, address: str):
        super().__init__(address, idn_query=False)
        self.reset()
        self.select_function(1)
        self.auto_trigger(True)

    def get_freq(self) -> float:
        return float(self.instr.read_bytes(21).decode('ascii')[4:-2])

    def auto_trigger(self, state: bool):
        self.instr.write('AU' + str(int(state)))

    def select_function(self, number: int):
        self.instr.write('FN' + str(number))

    def reset(self):
        self.instr.write('RE')

def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%b%d_%H%M%S")
    return now

def CSV_Init(header):
    name = input('Tester name:')
    SN = input('Unit serial number:')
    global workbook, FILENAME
    current_time = timestamp()
    FILENAME = SN + '_' + current_time[1] + '.csv'
    with open(FILENAME, 'w', newline='') as f:
        workbook = csv.writer(f)
        workbook.writerow(['Tester:', name])
        workbook.writerow(['Unit:', SN])
        workbook.writerow(['Date:', current_time[0]])
        workbook.writerow([])
        workbook.writerow(HEADER)

class TemperatureProbe():
    def __init__(self, address: str):
        temp = address.split(':')
        self.ip = temp[0]
        self.port = temp[1]
        self.channel_1 = '*SRTC'
        self.channel_2 = '*SRHC'
        self.bytes = 16
        self.server_address = (str(self.ip), int(self.port))
        self.idn = 'OMEGA Engineering,iServer MicroServer iTCX,18010042,unknown'
        #self.mac = '0003340198A1'
        self.name = 'OMEGA'

    def get_temp(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(self.server_address)
        self.send(sock)
        reply = self.read(sock)
        sock.close()

        return reply

    def send(self, sock):
        sock.sendall(bytes(str(self.channel_2) + '\r\n', 'utf-8'))

    def read(self, sock):
        word = ''
        received = sock.recv(self.bytes)
        for item in received:
            character = chr(item)
            word += character

        return word

    def close(self):
        pass

def main():
    rm = pyvisa.ResourceManager()

    # initialize equipment
    t_sensor = TemperatureProbe(TSENSOR_VISA)
    print("Temperature Sensor connected")

    f_counter = FrequencyCounter(FCOUNTER_VISA)
    print("Frequency Counter connected")

    dmm = Voltmeter(DMM_VISA)
    print("DMM connected")

    # initialize .csv
    CSV_Init(HEADER)

    # start testing
    prev_time = 0
    input('Press Enter to begin test')
    print(HEADER)
    while True:
        time.sleep(1)
        timer = time.time()
        if (timer - prev_time) < (PERIOD):
            continue
        current_time = timestamp()[0]
        prev_time = timer
        #----------------Do function-------------------

        # temperature
        try:
            temperature = t_sensor.get_temp()
        except:
            temperature = 'read error'

        # voltage
        try:
            voltage = dmm.get_voltage()
        except:
            voltage = 'read error'

        # frequency
        try:
            frequency = f_counter.get_freq()
            ppb = round(((frequency - 10000000) * 100), 4)
        except:
            frequency = 'read error'
            ppb = ''

        #----------------------------------------------

        # log data
        log = [current_time, frequency, ppb, voltage, temperature]
        print(log)
        with open(FILENAME, 'a', newline='') as f:
            workbook = csv.writer(f)
            workbook.writerow(log)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Keyboard interrupt detected. Stopping program')
        exit()
