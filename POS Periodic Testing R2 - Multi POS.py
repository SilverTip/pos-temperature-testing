#POS Periodic Testing R1
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#POS Periodic Testing

import pyvisa as visa
import csv
import sys
import time
import math
import json
import os
import shutil
import datetime

import Thermotron8200PlusInterface

GATE_TIME = 10
PERIOD = 60
HEADER = ['Time','POS1 Frequency', 'POS1 PPB','POS2 Frequency', 'POS2 PPB']

#TChamber_VISA = 'TCPIP0::10.0.10.151::8888::SOCKET'
FCOUNTER_VISA = 'TCPIP0::10.0.10.120::inst0::INSTR'
rm = visa.ResourceManager()
FCOUNTER = rm.open_resource(FCOUNTER_VISA)
FCOUNTER.timeout = 20000
print ("Frequency Counter connected")
#TChamber = rm.open_resource(TChamber_VISA)
#TChamber.timeout = 10000
#print ("Temperature Chamber connected")

def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%b%d_%H%M%S")
    return now

def CSV_Init(header):
    name = input('Tester name:')
    SN = input('Unit serial number:')
    global workbook, FILENAME
    current_time = timestamp()
    FILENAME = SN + '_' + current_time[1] + '.csv'
    with open(FILENAME, 'w', newline='') as f:
        workbook = csv.writer(f)
        workbook.writerow(['Tester:', name])
        workbook.writerow(['Unit:', SN])
        workbook.writerow(['Date:', current_time[0]])
        workbook.writerow([])
        workbook.writerow(HEADER)

def main():
    
    FCOUNTER.write(':CONFigure:SCALar:VOLTage:FREQuency (%s)' % ('@1'))
    FCOUNTER.write(':CONFigure:SCALar:VOLTage:FREQuency (%s)' % ('@2'))
    CSV_Init(HEADER)
    prev_time = 0
    input('Press Enter to begin test')
    print(HEADER)
    while(1):       
        timer = time.time()
        if((timer - prev_time) > (PERIOD)):
            current_time = timestamp()[0]
            prev_time = timer          
            #----------------Do function-------------------
            FCOUNTER.write('CONF:FREQ (@1)')
            FCOUNTER.write(':SENSe:FREQuency:GATE:TIME %G' % (GATE_TIME))
            time.sleep(1)
            value1 = float(FCOUNTER.query_ascii_values('READ?')[0])
            time.sleep(1)
            ppb1 = round(((value1 - 10000000) * 100), 4)
            time.sleep(GATE_TIME)
            FCOUNTER.write('CONF:FREQ (@2)')
            FCOUNTER.write(':SENSe:FREQuency:GATE:TIME %G' % (GATE_TIME))
            time.sleep(1)
            value2 = float(FCOUNTER.query_ascii_values('READ?')[0])
            time.sleep(1)
            ppb2 = round(((value2 - 10000000) * 100), 4)
            #temp = float(Thermotron8200PlusInterface.TChamberReadWrite(TChamber, 'PVAR1?'))
            
            #----------------------------------------------
            log = [current_time, value1, ppb1, value2, ppb2]
            print(log)
            with open(FILENAME, 'a', newline='') as f:
                workbook = csv.writer(f)
                workbook.writerow(log)



try:
    main()
except KeyboardInterrupt:
    print('Keyboard interrupt detected. Stopping program')
    exit()
