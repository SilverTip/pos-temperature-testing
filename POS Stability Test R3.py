## Orbital Research Ltd.
## Upgraded to Python 3.9.7
##-----------------------------------------------------------
import sys
import pyvisa as visa
import time
import csv
import datetime
import socket
import xlsxwriter
##-----------------------------------------------------------
##2021-11-08 (POS Stability Test R1.py) - author Bill
##2022-04-13 (POS Stability Test R3.py) - reviewd by Benjamin Stadnik - updated to Python 3, checked fuctionality. Removed instances of workboon.open() and workbook.close(). Only generates file at end of test.

#TESTING PARAMETERS
######################################################
GATE_TIME = 5.0 #Set gate time for frequency counter
DELAY = 30 #Polling period
Err_threshold = 0.5 #0.5ppb
Err_threshold2 = 5 #5 ppb

#FUNCTIONS
######################################################
def timestamp():
    now = datetime.datetime.now()
    now = now.strftime("%d %b %Y %H:%M:%S")
    return now

def Get_Temp(): #Used to communicate with temperature probe on prod rack #1
    # Connect the socket to the port where the server is listening
    server_address = ('10.0.10.117', 2000)
    message = '*SRHC\r'
    message_byte = str.encode(message)
    try:
        print('connecting to %s port %s' % server_address)
        #sock.close()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(server_address)       
        # Send data

        print('sending "%s"' % message)
        sock.sendall(message_byte)

        # Look for the response
        amount_received = 0
        amount_expected = len(message)
        
        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
        
    finally:
        sock.close()
        T_Chamber = data.strip()
        T_Chamber.lstrip()
        T_Chamberstr = T_Chamber.decode()
        
    return T_Chamberstr

#SCRIPT START
######################################################

#Workbook parameters
SERIAL_NUMBER = input("Device Serial Number: ")
TESTER = input("Tester Name: ")
rm = visa.ResourceManager()

#Connect to temperature sensor
print ("Temperature sensor connected")

#Connect to Multimeter
cur = rm.open_resource('GPIB4::24::INSTR')
cur.timeout = 600000
print ("Digital Multimeter connected")

#Connect to Frequency Counter
v53220A = rm.open_resource('USB0::0x0957::0x1807::MY50010326::0::INSTR')
v53220A.timeout = 60000
print ("Frequency Counter connected")
 
v53220A.write('*RST')     
v53220A.write('*CLS') 

#Create Workbook
TIMEFORMAT  = "%Y%m%d-%H%M" 
DATE        =  time.strftime(TIMEFORMAT, time.localtime(time.time()))
TITLE       = "POS Stability Test "
workbook = xlsxwriter.Workbook(SERIAL_NUMBER + '_' + TITLE + '(' + DATE + ').xlsx')
worksheet = workbook.add_worksheet()

#Write Workbook Headers
worksheet.write(0, 0, 'S/N:')
worksheet.write(1, 0, 'Tested by:')
worksheet.write(0, 1, str(SERIAL_NUMBER))
worksheet.write(1, 1, str(TESTER))
worksheet.write(2, 0, 'Time')
worksheet.write(2, 1, 'POS Frequency (Hz)')
worksheet.write(2, 2, 'Freq Offset (ppb)')
worksheet.write(2, 3, 'Current (A)')
worksheet.write(2, 4, 'Temperature (C)')
#workbook.close()  
dummy = input("Press Enter to Begin Test\n")
print('\n Press "CTRL + C" to stop test\n')   
print('Time : Frequency(Hz) : Offset(ppb) : Current(A) : Temperature(C)')

row = 3
prev_time = 0
    
while(1):   

    try:
        timer = time.time()

        #Read monitoring equipment every DELAY seconds
        if((timer - prev_time) > DELAY):

            prev_time = timer

            #Fetch Time
            current_time = timestamp()

            #Fetch Temperature Value
            temperature = Get_Temp()

            #Fetch DC Current Value
            temp_curr = cur.query_ascii_values('current')
            current = temp_curr[0]
            current = round(current, 3)
            
            #Fetch Channel 1 Frequency
                       
            v53220A.write(':CONFigure:SCALar:VOLTage:FREQuency (%s)' % ('@1'))
            v53220A.write(':SENSe:FREQuency:GATE:TIME %G S' % (GATE_TIME))
            temp_values = v53220A.query_ascii_values(':READ?')
            freq1 = temp_values[0]

            #Get frequency offset from 10MHz
            scale1 = (freq1 - 10000000) * 100
            s1 = round(scale1, 3)
            
            #Write to Spreadsheet
            #workbook.open()

            #worksheet.write(row, column, column_name)
            worksheet.write(row, 0, current_time)
            worksheet.write(row, 1, freq1)
            worksheet.write(row, 2, s1)
            worksheet.write(row, 3, current)
            worksheet.write(row, 4, temperature)

            worksheet.write(row, 6, Err_threshold)
            worksheet.write(row, 7, (Err_threshold * -1))
            worksheet.write(row, 8, Err_threshold2)
            worksheet.write(row, 9, (Err_threshold2 * -1))

            row = row + 1
            #workbook.close()            

            #Print Values to Console
            print('MONITOR:', current_time, freq1, s1, current, temperature)
            #print('MONITOR:', current_time, temperature) #freq1, s1, current 

        time.sleep(DELAY)


    except KeyboardInterrupt:
        error = "KeyboardInterrupt stopping the program..."
        print (error)
        worksheet.write(row, 0, error)
        workbook.close()
        break
