#Thermotron8200PlusInterface.py
#Written by Benjamin Stadnik
#Orbital Research Ltd.
#2021-07-12

import pyvisa as visa

TChamber_VISA = 'TCPIP0::10.0.10.151::8888::SOCKET'

def Init(VISA):
    global TChamber, rm
    rm = visa.ResourceManager()
    TChamber = rm.open_resource(TChamber_VISA)
    TChamber.timeout = 10000

def TChamberReadWrite(inst, cmd): #Manually read byte string becasue visa.read_raw() and visa.query_ascii_values() doesn't work
    character = ''
    string = ''
    inst.write(cmd)
    while(character != '\n'):    
        character = inst.read_bytes(1)
        character = character.decode("utf-8")
        string += character       
    return string

def main():
    message = ''
    while(True):
        cmd = input('Enter Command:')
        try:
            message = TChamberReadWrite(TChamber, cmd)
        except Exception as e:
            print('Failure to communicate.')
            print(e)
        print(message)

#Run main if used as standalone application
if __name__ == '__main__':
    Init(TChamber_VISA)
    main()


#Useful commands: (All channel 1 commands)
#--------------------------
#SETP1,[temperature] - set temperature setpoint
#STOP - stop temperature cycle
#RUNM - run chamber in manual mode
#HOLD - hold current temp (even if no setpoint)
#RESM - Resume manual mode (from hold function)
#MRMP1,[ramp rate] - set manual ranmp rate (degree/min)
